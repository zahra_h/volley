package com.example.volley;

public class URLMaker {

    private String baseUrl = "http://api.openweathermap.org/data/2.5/weather?q=";
    private String appId = "&appid=307b5a9886c80231f588b39256b594f3";
    private String lastPartUrl = "&units=metric";
    private String city, country;

    public URLMaker(String city, String country){
        this.city = city;
        this.country = country;
    }

    public URLMaker(String city){
        this.city = city;
    }

    public String setURL(){
        String url = "";
//        if (!country.equals("")){
            if(!city.equals("")){
                url = baseUrl + city + appId + lastPartUrl;
            }
//        }
        return url;
    }
}

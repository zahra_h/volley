package com.example.volley;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class WeatherActivity extends AppCompatActivity implements View.OnClickListener {

    private Button forecastBtn;
    private Spinner citySpinner;
    private FloatingActionButton fab;
    private ImageView errorConnectionImg;
    private TextView errorConnectionText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        init();
        if (isNetworkConnected()[0] == "0") {
            errorConnectionImg.setVisibility(View.VISIBLE);
            errorConnectionImg.setColorFilter(R.color.black, PorterDuff.Mode.SRC_ATOP);
            errorConnectionText.setVisibility(View.VISIBLE);
            citySpinner.setVisibility(View.GONE);
            forecastBtn.setVisibility(View.GONE);
        } else {
            errorConnectionImg.setVisibility(View.GONE);
            errorConnectionText.setVisibility(View.GONE);
            citySpinner.setVisibility(View.VISIBLE);
            forecastBtn.setVisibility(View.VISIBLE);

            String[] cityArray = {"Kabul", "Baku", "Manama", "Brasilia", "Mexico City", "Monaco", "Muscat", "Islamabad", "Ottawa",
                    "Santiago", "Beijing", "San Jose", "Lima", "Doha", "Copenhagen", "Cairo", "Madrid", "Stockholm", "New Delhi",
                    "Rome", "Ankara", "Tokyo" ,"Qom", "London"};
            spinnerSetup(cityArray, citySpinner);
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.forecastBtn:
//                if (!countrySpinner.getSelectedItem().toString().equals("")){
                    if (!citySpinner.getSelectedItem().toString().equals("")){
                        Intent intent = new Intent(WeatherActivity.this, MainActivity.class);
                        intent.putExtra("city", citySpinner.getSelectedItem().toString());
                        startActivity(intent);
                    }
//                }
                break;
            default:
                break;
        }
    }

    public void init(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        forecastBtn = findViewById(R.id.forecastBtn);
        citySpinner = findViewById(R.id.city_spinner);
        forecastBtn.setOnClickListener(this);
        fab = findViewById(R.id.fab);
        errorConnectionImg = findViewById(R.id.error_connection_img);
        errorConnectionText = findViewById(R.id.error_connection_text);
    }

    public void spinnerSetup(String[] array, Spinner spinner){
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, array);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);
    }

    private String[] isNetworkConnected() {
        String[] connections = new String[2];
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if (isConnected) {
            connections[0] = "1";
            boolean isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
            if (isWiFi)
                connections[1] = "1";
            else
                connections[1] = "2";
        }else
            connections[0] = "0";

        return connections;
    }

}
